package com.example.tp1;

import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity ;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class AnimalActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal);


        if(getIntent().getSerializableExtra("Animal") != null){
            Animal animal = (Animal) getIntent().getSerializableExtra("Animal");

            ImageView imageView_Animal = findViewById(R.id.imageView_Animal);

            //Display Image
            String uri = "@drawable/"+animal.getImgFile();
            int imageResource = getResources().getIdentifier(uri, null, getPackageName());
            Drawable res = getResources().getDrawable(imageResource);
            imageView_Animal.setImageDrawable(res);


            TextView txtView_Name = findViewById(R.id.txtView_Name);
            final String AnimalName = getIntent().getSerializableExtra("AnimalName").toString();
            txtView_Name.setText(AnimalName);


            TextView vie = findViewById(R.id.vie);
            vie.setText(animal.getStrHightestLifespan());

            TextView periode = findViewById(R.id.periode);
            periode.setText(animal.getStrGestationPeriod());

            TextView PoidsN = findViewById(R.id.PoidsN);
            PoidsN.setText(animal.getStrBirthWeight());

            TextView PoidsA = findViewById(R.id.PoidsA);
            PoidsA.setText(animal.getStrAdultWeight());

            final EditText status = findViewById(R.id.status);
            status.setText(animal.getConservationStatus());

            Button enregistrer = findViewById(R.id.enregistrer);
            enregistrer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AnimalList.getAnimal(AnimalName).setConservationStatus(status.getText().toString());
                }
            });



        }


    }
}
