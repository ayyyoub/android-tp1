package com.example.tp1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.tp1.R;


public class MainActivity extends AppCompatActivity {
    String[] Animales = AnimalList.getNameArray();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        ListView  listView = (ListView) findViewById(R.id.list);

        ArrayAdapter<String> Adapter =
                new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, Animales);

        listView.setAdapter(Adapter);

     /*   final Intent intent = new Intent(this,CountryActivity.class);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final String item = (String)parent.getItemAtPosition(position);
                Bundle bn = new Bundle();
                bn.putString("cnt",item);
                intent.putExtra("key",bn);
                startActivity(intent);

        });*/
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                // Getting listview click value into String variable.
              //  String TempListViewClickedValue = .toString();

                Intent intent = new Intent(MainActivity.this, AnimalActivity.class);

                // Sending value to another activity using intent.
                intent.putExtra("ListViewClickedValue", Animales[position]);

                startActivity(intent);

            }
        });

    }

    }

